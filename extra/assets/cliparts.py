from thht_manim.images import PNGImage
from manim import *


class Apple(PNGImage):
    def __init__(self):
        super().__init__(file_name='apple')
        self.scale_to_fit_height(0.7)


class Albert(PNGImage):
    def __init__(self):
        super().__init__(file_name='albert')
        self.scale_to_fit_height(3)


class Darla(PNGImage):
    def __init__(self):
        super().__init__(file_name='darla')
        self.scale_to_fit_height(3)


class Bitcoin(PNGImage):
    def __init__(self):
        super().__init__(file_name='bitcoin')
        self.scale_to_fit_height(0.7)


class SpeechBubble(SVGMobject):
    def __init__(self, direction=LEFT,
                 center_point=ORIGIN,
                 content_scale_factor=0.75,
                 bubble_center_adjustment_factor=1.0 / 8,
                 **kwargs):
        self.direction = direction
        self.center_point = center_point
        self.content_scale_factor = content_scale_factor
        self.bubble_center_adjustment_factor = bubble_center_adjustment_factor
        super().__init__(file_name='assets/svg_images/Bubbles_speech.svg', color=WHITE,
                         fill_opacity=0)

        self.center()
        self.stretch_to_fit_height(self.height)
        self.stretch_to_fit_width(self.width)
        if self.direction[0] > 0:
            self.flip()
        self.direction_was_specified = "direction" in kwargs
        self.content = Mobject()

    def get_tip(self):
        # TODO, find a better way
        return self.get_corner(DOWN + self.direction) - 0.6 * self.direction

    def get_bubble_center(self):
        factor = self.bubble_center_adjustment_factor
        return self.get_center() + factor * self.get_height() * UP

    def move_tip_to(self, point):
        mover = VGroup(self)
        if self.content is not None:
            mover.add(self.content)
        mover.shift(point - self.get_tip())
        return self

    def flip(self, axis=UP):
        Mobject.flip(self, axis=axis)
        if abs(axis[1]) > 0:
            self.direction = -np.array(self.direction)
        return self

    def pin_to(self, mobject):
        mob_center = mobject.get_center()
        want_to_flip = np.sign(mob_center[0]) != np.sign(self.direction[0])
        can_flip = not self.direction_was_specified
        if want_to_flip and can_flip:
            self.flip()
        boundary_point = mobject.get_critical_point(UP - self.direction)
        vector_from_center = 1.0 * (boundary_point - mob_center)
        self.move_tip_to(mob_center + vector_from_center)
        return self

    def position_mobject_inside(self, mobject):
        scaled_width = self.content_scale_factor * self.get_width()
        if mobject.get_width() > scaled_width:
            mobject.set_width(scaled_width)
        mobject.shift(self.get_bubble_center() - mobject.get_center())
        return mobject

    def add_content(self, mobject):
        self.remove(self.content)
        self.position_mobject_inside(mobject)
        self.content = mobject
        self.add(self.content)
        return self.content

    def write(self, *text):
        self.add_content(Tex(*text))
        return self

    def resize_to_content(self):
        target_width = self.content.get_width()
        target_width += max(MED_LARGE_BUFF, 2)
        target_height = self.content.get_height()
        target_height += 2.5 * LARGE_BUFF
        tip_point = self.get_tip()
        self.stretch_to_fit_width(target_width)
        self.stretch_to_fit_height(target_height)
        self.move_tip_to(tip_point)
        self.position_mobject_inside(self.content)

    def clear(self):
        self.add_content(VMobject())
        return self


class Chocolate(PNGImage):
    def __init__(self):
        super().__init__(file_name='chocolate')
        self.scale_to_fit_height(0.7)


class LeftHalfChocolate(Chocolate):
    def process_image(self, im):
        return im.crop((0, 0, im.width/2, im.height))


class RightHalfChocolate(Chocolate):
    def process_image(self, im):
        return im.crop((im.width/2, 0, im.width, im.height))
