from manim import NumberLine as ManimNumberLine
from manim import VGroup


class NumberLine(ManimNumberLine):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.tick_marks = VGroup()
        self.add(self.tick_marks)

        self.last_added_ticks = list()
        self.last_added_numbers = list()
        self.numbers = None

    def add_ticks(self, *numbers):
        self.last_added_ticks = list()

        for cur_number in numbers:
            new_tick = self.get_tick(cur_number, self.tick_size)
            self.last_added_ticks.append(new_tick)
            self.tick_marks.add(new_tick)

        return self

    def add_single_numbers(self, *numbers, **kwargs):
        this_added_numbers = self.get_number_mobjects(*numbers, **kwargs)
        if self.numbers is None:
            self.numbers = this_added_numbers
        else:
            for cur_number in this_added_numbers:
                self.numbers.add(cur_number)

        self.add(this_added_numbers)
        self.last_added_numbers = this_added_numbers.submobjects.copy()

        return self


    def add_numbers_with_tick(self, *numbers):
        self.add_single_numbers(*numbers)
        self.add_ticks(*numbers)

        return self

