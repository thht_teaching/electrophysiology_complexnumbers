from manim import *
import math

from thht_manim.geometry import BetterVector


class ComplexPlane(VGroup):
    def __init__(self, min_real=None, max_real=10, min_imag=None,
                 max_imag=10,
                 tick_size=0.1,
                 number_scale_val=0.5,
                 decimal_number_config=None,
                 line_to_number_buff=MED_SMALL_BUFF,
                 numbers_per_unit=2,
                 upper_buff=2,
                 **kwargs):

        if decimal_number_config is None:
            decimal_number_config = {
                'num_decimal_places': 0,
            }

        self.decimal_number_config = decimal_number_config
        self.tick_size = tick_size
        self.number_scale_val = number_scale_val
        self.line_to_number_buff = line_to_number_buff
        self.numbers_per_unit = numbers_per_unit

        super().__init__(**kwargs)

        self.lines = {
            'positive_real': None,
            'negative_real': None,
            'positive_imag': None,
            'negative_imag': None
        }
        self.ticks = VDict()
        self.numbers = VDict()
        self.arrow = None
        self.length_brace = None

        if min_real is None:
            min_real = -max_real

        if min_imag is None:
            min_imag = -max_imag

        self.max_real = max_real
        self.min_real = min_real
        self.max_imag = max_imag
        self.min_imag = min_imag


        self.min_coord = max(-config['frame_x_radius'],
                             -config['frame_y_radius']) + upper_buff / 2
        self.max_coord = min(config['frame_x_radius'],
                             config['frame_y_radius']) - upper_buff / 2

        self.center = (self.min_coord + self.max_coord) / 2

        self.shift_indicator = Circle([0, -upper_buff/2, 0],
                                      color=BLACK)

        self.add(self.shift_indicator)
        self.add(self.ticks)
        self.add(self.numbers)

    @property
    def current_shift(self):
        return self.shift_indicator.get_center()

    def create_complex_plane(self, imag_suffix='i'):
        self.add_line(positive=True, real=True)
        self.add_line(positive=False, real=True)
        self.add_line(positive=True, real=False)
        self.add_line(positive=False, real=False)
        self.add_ticks(self.get_max_range())
        self.add_ticks(self.get_max_range(), imag=True,
                       exclude_zero=True, imag_suffix=imag_suffix)

    def get_max_number(self):
        return math.floor((self.max_coord - DEFAULT_ARROW_TIP_LENGTH) * self.numbers_per_unit)

    def get_max_range(self):
        return range(-self.get_max_number(), self.get_max_number() + 1)

    def add_line(self, positive, real, return_animation=False, return_line=False):
        if positive:
            pos_string = 'positive'
        else:
            pos_string = 'negative'

        if real:
            real_string = 'real'
        else:
            real_string = 'imag'

        if positive:
            start = self.center
            end = self.max_coord
        else:
            start = self.center
            end = self.min_coord

        if real:
            start *= RIGHT
            end *= RIGHT
        else:
            start *= UP
            end *= UP

        this_line = Line(start + self.current_shift, end + self.current_shift)

        this_tip = this_line.create_tip()
        this_line.add_tip(this_tip)

        self.lines['%s_%s' % (pos_string, real_string)] = this_line
        self.add(this_line)

        if return_animation:
            rval = FadeIn(this_line)
        elif return_line:
            rval = this_line
        else:
            rval = self

        return rval

    def get_current_lines(self):
        return VGroup(*[l for l in self.lines.values() if l is not None])

    def add_imag_line(self):
        self.add_line(positive=True, real=False)
        self.add_line(positive=False, real=False)

        return self

    def _create_tick(self, tick, imag=False, tick_label=None, imag_suffix='i', extra_number_space=0):
        direction = DOWN
        if imag:
            tick = tick * 1j
            direction = RIGHT

        coord = self.number_to_point(tick)
        tick_str = str(tick)
        if imag:
            tick = tick.imag

        this_tick = Line(self.tick_size * DOWN, self.tick_size * UP)
        if imag:
            this_tick.rotate(90 * DEGREES)

        this_tick.move_to(coord)
        this_tick.match_style(self)

        buff = self.line_to_number_buff + extra_number_space

        if tick_label is not None:
            this_number = MathTex(tick_label)
            this_number.scale(self.number_scale_val)
            this_number.next_to(coord,
                                direction=direction,
                                buff=buff)
        else:
            this_number = self.get_number_mobject(tick, imag=imag,
                                                  imag_suffix=imag_suffix,
                                                  buff=buff)

        return this_tick, this_number, tick_str

    def add_tick(self, tick, tick_label=None, imag=False, imag_suffix='i', return_animation=False, extra_number_space=0):
        this_tick, this_number, tick_str = self._create_tick(tick, imag, tick_label, imag_suffix, extra_number_space)

        self.ticks[tick_str] = this_tick
        self.numbers[tick_str] = this_number

        if return_animation:
            rval = FadeIn(VGroup(this_tick, this_number))
        else:
            rval = VGroup(this_tick, this_number)

        return rval

    def add_ticks(self, ticks, tick_labels=None, imag=False, imag_suffix='i', exclude_zero=False, return_animation=False, extra_number_space=0):
        anims = list()

        for idx_t, t in enumerate(ticks):
            if not exclude_zero or t != 0:
                if tick_labels is None:
                    tl = None
                else:
                    tl = tick_labels[idx_t]
                this_anim = self.add_tick(t, tick_label=tl, imag=imag, imag_suffix=imag_suffix, return_animation=return_animation, extra_number_space=extra_number_space)
                anims.append(this_anim)

        if return_animation:
            rval = AnimationGroup(*anims)
        else:
            rval = VGroup(*anims)

        return rval

    def remove_tick(self, tick):
        try:
            self.ticks.remove(str(tick))
            self.numbers.remove(str(tick))
        except KeyError:
            pass

        return self

    def _number_to_point(self, number):
        p = float(number - self.min_coord) / (self.max_coord - self.min_coord)

        p = interpolate(self.min_coord, self.max_coord, p)
        p /= self.numbers_per_unit

        return p

    def number_to_point(self, number):
        number = complex(number)

        p_real = self._number_to_point(number.real) * RIGHT
        p_imag = self._number_to_point(number.imag) * UP

        p = p_real + p_imag + self.current_shift

        return p

    def n2p(self, number):
        return self.number_to_point(number)

    def get_number_mobject(self, number, number_config=None, scale_val=None,
                           direction=None, buff=None, imag=False,
                           imag_suffix='* i'):
        number_config = merge_dicts_recursively(
            self.decimal_number_config,
            number_config or {},
            )
        if scale_val is None:
            scale_val = self.number_scale_val
        if direction is None:
            if imag:
                direction = RIGHT
            else:
                direction = DOWN

        buff = buff or self.line_to_number_buff

        if imag:
            unit = imag_suffix
        else:
            unit = None

        num_mob = DecimalNumber(number, unit=unit, **number_config)
        num_mob.scale(scale_val)

        if imag:
            number = number * 1j

        num_mob.next_to(self.number_to_point(number), direction=direction, buff=buff)
        return num_mob

    def add_length_brace(self, start, end, return_animation=False):
        start_tick = self._create_tick(start)[0]
        end_tick = self._create_tick(end)[0]

        target_for_brace = VGroup(start_tick, end_tick)

        if self.length_brace is None:
            br = Brace(target_for_brace, UP)
            br_text = Variable(end - start, 'Length', var_type=Integer).scale(0.5)
            br.put_at_tip(br_text)
            self.length_brace = VGroup(br, br_text)
            self.add(self.length_brace)

            if return_animation:
                return FadeIn(self.length_brace)
            else:
                return self.length_brace

        else:
            br = self.length_brace[0]
            br_text = self.length_brace[1]
            left = target_for_brace.get_corner(DOWN + LEFT)
            right = target_for_brace.get_corner(DOWN + RIGHT)
            target_width = right[0] - left[0]

            fun_br = lambda b: b.stretch_to_fit_width(target_width).match_x(start_tick, LEFT)
            fun_text = lambda t: t.match_x(target_for_brace)

            if return_animation:
                return AnimationGroup(
                    ApplyFunction(fun_br, br),
                    ApplyFunction(fun_text, br_text),
                    ApplyMethod(br_text.tracker.set_value, end - start)
                )
            else:
                fun_br(br)
                fun_text(br_text)
                br_text.tracker.set_value(end - start)
                return self.length_brace

    def old_add_length_brace(self, start, end, return_animation=False):
        old_brace = self.length_brace


        start_tick = self._create_tick(start)[0]
        end_tick = self._create_tick(end)[0]
        br = Brace(VGroup(start_tick, end_tick), UP)

        br_text = br.get_text('Length = %d' % (end-start, ), buff=SMALL_BUFF).scale(0.5)

        self.length_brace = VGroup(br, br_text)
        #self.add(self.length_brace)

        if old_brace is None:
            anim = FadeIn(self.length_brace)
        else:
            #self.remove(old_brace)
            anim = ReplacementTransform(old_brace, self.length_brace)

        if return_animation:
            rval = anim
        else:
            rval = self

        return rval

    def remove_length_brace(self):
        self.remove(self.length_brace)
        self.length_brace = None

        return self

    def set_arrow(self, number, return_animation=False):
        if self.arrow is None:
            self.arrow = Arrow(0 * DOWN, 0.4 * UP,
                               max_stroke_width_to_length_ratio=12)
            self.arrow.set_x(self.n2p(0)[0])
            self.arrow.next_to(self.n2p(0), direction=DOWN,
                               buff=self.line_to_number_buff * 3)
            self.arrow.set_x(self.n2p(number)[0])
            self.add(self.arrow)

            if return_animation:
                return FadeIn(self.arrow)
            else:
                return self.arrow

        else:
            if return_animation:
                return ApplyMethod(self.arrow.set_x, self.n2p(number)[0])
            else:
                self.arrow.set_x(self.n2p(number)[0])
                return self.arrow


    def remove_arrow(self, return_animation=False):
        self.remove(self.arrow)

        if return_animation:
            rval = FadeOut(self.arrow)
        else:
            rval = self

        self.arrow = None

        return rval

    def get_pointer_to_number(self, number, arrow_length=2, arrow_angle=0,
                              number_scale=0.75, num_decimal_places=0,
                              align_direction=RIGHT):
        number_object = DecimalNumber(number,
                                      num_decimal_places=num_decimal_places)
        number_object.scale(number_scale)
        arrow = Arrow(self.n2p(number) + arrow_length * RIGHT, self.n2p(number),
                      buff=0)
        arrow.rotate(arrow_angle, about_point=arrow.get_end())

        number_object.next_to(arrow.get_start(), align_direction)

        return VGroup(arrow, number_object)

    def get_vector(self, start=0, end=1+1j, **kwargs):
        return BetterVector(self.n2p(start), self.n2p(end), **kwargs)


