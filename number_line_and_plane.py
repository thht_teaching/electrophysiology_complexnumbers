import math
from copy import deepcopy

from manim import *
import sys
import os
import numpy as np
from thht_manim.animations import BetterSuccession
from thht_manim.geometry import BetterVector
from thht_manim.numbers import FlexibleVariable, ColoredComplexNumber

sys.path.append(os.getcwd())

from thht_manim.scene import (SceneOrderedPartials, AddSlideNumber, SlideScene,
                              SplitScene, AddExtraFrame, DefaultSectionScene)
from thht_manim.images import PNGImage
from thht_manim.slides import SlideHeader, SlideText, SlideTextMarkdown
from extra.assets.cliparts import Apple, Albert, Bitcoin, SpeechBubble, \
    Chocolate, Darla, LeftHalfChocolate, RightHalfChocolate
from extra.objects.complex_plane import ComplexPlane
# noinspection PyAttributeOutsideInit
class NumberLineAndPlane(SlideScene, AddSlideNumber, AddExtraFrame, DefaultSectionScene, Scene):
    max_number = 5
    big_math_kwargs = dict(
        tex_environment='align*',
        font_size=48*1.4,
        to_header_buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * 3,
        center_align=True,
        markdown=False
    )

    small_math_kwargs = deepcopy(big_math_kwargs)
    small_math_kwargs['font_size'] = 0.8 * 48

    numberline_shift = np.array([0, -2, 0])

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sbdl_logo = PNGImage(file_name='sbdl_logo')
        self.sbdl_logo.scale(0.06)
        self.sbdl_logo.to_corner(UP + LEFT, buff=DEFAULT_MOBJECT_TO_EDGE_BUFFER / 2)
        self.add(self.sbdl_logo)

        self.plus_logo = PNGImage(file_name='plus_logo')
        self.plus_logo.match_height(self.sbdl_logo)
        self.plus_logo.to_corner(UP + RIGHT, buff=DEFAULT_MOBJECT_TO_EDGE_BUFFER / 2)
        self.add(self.plus_logo)

        self.current_numberset = None
        self.complex_plane = None
        self.to_remove = VGroup()

    def part01_title_page(self):
        title_text = SlideHeader('Complex Numbers')
        title_text.set_y(2)
        subtitle_text = Tex('an Introduction\\\\Thomas Hartmann')

        self.add(title_text, subtitle_text)

        self.wait()

        self.remove(title_text, subtitle_text)

    def part02_overview(self):
        self.set_header('Complex Numbers')

        self.set_text(r'''
* Extension of the number range of "real numbers"
* Facilitates representation and calculation of things that have:
    * 1 non-circular dimension:
        * distance, power, elapsed time...
    * 1 circular dimension:
        * phase, angles, time of day...
    * Like oscillations
* Used throughout signal processing
* Good to have a basic understanding
''')

        self.wait()

    def part03_pippi(self):
        self.set_header('The most important thing to remember today')
        self.remove_text()
        pippi = PNGImage(file_name='pippi')
        text = SlideText('Math is about defining and adapting logical '
                         'systems to describe and represent the world.',
                         alignment='\\raggedright')
        text.next_to(pippi, DOWN, buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * 2)

        self.add(pippi, text)

        self.wait()

        self.remove(pippi, text)

    def part04_albert_and_the_line(self):
        self.set_header('You all started like this')
        ns_anim = self.set_numberset('N')

        self.albert = Albert()
        self.albert.set_y(1)

        self.complex_plane = ComplexPlane()

        self.complex_plane.add_line(positive=True, real=True)
        self.complex_plane.shift(self.numberline_shift)

        self.play(FadeIn(self.albert),
                  FadeIn(self.complex_plane),
                  ns_anim)

    def part05_albert_gets_his_first_apple(self):
        self.apple1 = Apple()
        self.apple1.next_to(self.albert, RIGHT)

        self.play(self.complex_plane.add_tick(1, return_animation=True),
                  FadeIn(self.apple1, target_position=RIGHT, shift=LEFT),
                  self.complex_plane.set_arrow(1, return_animation=True))

    def part06_the_second_apple_and_the_distance(self):
        self.apple2 = Apple()
        self.apple2.next_to(self.apple1, RIGHT)

        self.play(self.complex_plane.set_arrow(2, return_animation=True),
                  FadeIn(self.apple2, target_position=RIGHT, shift=LEFT))

        self.complex_plane.add_tick(2)
        self.play(self.complex_plane.add_tick(2, return_animation=True))

        self.play(self.complex_plane.add_length_brace(1, 2, return_animation=True))

    def part07_the_third_apple(self):
        self.apple3 = Apple()
        self.apple3.next_to(self.apple2)
        self.apple3.set_x(5)

        self.play(FadeIn(self.apple3, shift=LEFT, target_position=RIGHT), add_new_scene=False)
        self.play(AnimationGroup(
            self.complex_plane.add_length_brace(2, 3,
                                                return_animation=True),
            ApplyMethod(self.apple3.next_to, self.apple2),
            self.complex_plane.set_arrow(3, return_animation=True)
        ), add_new_scene=False)
        self.play(self.complex_plane.add_tick(3, return_animation=True))

    def part08_plustwo_apples(self):
        self.apple4 = Apple()
        self.apple4.next_to(self.apple2)
        self.apple4.set_x(5).shift(np.array([0, -1, 0]))
        self.apple5 = Apple()
        self.apple5.next_to(self.apple4)

        self.play(FadeIn(Group(self.apple4, self.apple5), target_position=RIGHT, shift=LEFT), add_new_scene=False)
        self.play(Succession(
            self.complex_plane.add_length_brace(1, 3,
                                                return_animation=True),
            self.complex_plane.add_length_brace(3, 5,
                                                return_animation=True)
        ), add_new_scene=False)
        self.play(
            AnimationGroup(
                ApplyMethod(Group(self.apple4, self.apple5).next_to,
                            self.apple2, DOWN),
                self.complex_plane.set_arrow(5, return_animation=True)
            ), add_new_scene=False)
        self.play(self.complex_plane.add_ticks([4, 5], return_animation=True))

    def part09_minus_3_apples(self):
        bad_apple_group = Group(self.apple3, self.apple4, self.apple5)
        self.play(Indicate(bad_apple_group))

        self.play(
            self.complex_plane.add_length_brace(2, 5, return_animation=True),
            add_new_scene=False)
        self.play(
            AnimationGroup(
                FadeOut(bad_apple_group, target_position=RIGHT, shift=RIGHT),
                self.complex_plane.set_arrow(2, return_animation=True)
            )
        )

    def part10_to_zero(self):
        bad_apple_group = Group(self.apple1, self.apple2)
        self.play(Indicate(bad_apple_group))

        self.play(
            self.complex_plane.add_length_brace(0, 2,
                                                return_animation=True),
            add_new_scene=False)
        self.play(
            AnimationGroup(
                FadeOut(bad_apple_group, shift=RIGHT),
                self.complex_plane.set_arrow(0, return_animation=True)
            ),
            add_new_scene=False)
        self.play(self.complex_plane.add_tick(0, return_animation=True))


    def part11_themoney(self):
        self.set_header('But then money came along...')

        self.bitcoins = Group(*[Bitcoin() for i in range(3)])
        self.bitcoins[0].next_to(self.albert, RIGHT)
        self.bitcoins[0].set_x(4)
        self.bitcoins[1].next_to(self.bitcoins[0])
        self.bitcoins[2].next_to(self.bitcoins[1])

        self.play(
            AnimationGroup(
                FadeOut(self.complex_plane.length_brace),
                FadeIn(self.bitcoins, target_position=RIGHT, shift=LEFT),
            ), add_new_scene=False)
        self.play(
            AnimationGroup(
                ApplyMethod(self.bitcoins.next_to, self.albert),
                self.complex_plane.set_arrow(3, return_animation=True)
            )
        )

    def part12_pay_2_bitcoins(self):
        self.speech_bubble = SpeechBubble()
        self.speech_bubble.scale(1.5)
        self.speech_bubble.next_to(self.albert, LEFT, buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * 3)
        self.speech_bubble.write('Give me two bitcoins!')

        self.play(Create(self.speech_bubble))

        self.play(self.complex_plane.add_length_brace(1, 3, return_animation=True), add_new_scene=False)
        self.play(
            AnimationGroup(
                FadeOut(
                    Group(*self.bitcoins[1:]), shift=LEFT
                ),
                self.complex_plane.set_arrow(1, return_animation=True)
            ), add_new_scene=False)
        self.play(ApplyMethod(self.speech_bubble.write, 'Thanks!'))

    def part13_pay_2_more_bitcoins(self):
        minus_bitcoin = self.bitcoins[0].copy()
        minus_text = Tex('-')
        minus_text.next_to(minus_bitcoin, LEFT)

        ns_anim = self.set_numberset('Z')

        self.minus_group = Group(minus_text, minus_bitcoin)
        self.play(
            ApplyMethod(self.speech_bubble.write, 'Give me two more bitcoins!\\\\You get them back tomorrow!')
        )

        self.play(self.complex_plane.add_length_brace(-1, 1,
                                                      return_animation=True),
                  add_new_scene=False)
        self.play(
            AnimationGroup(
                self.complex_plane.set_arrow(-1, return_animation=True),
                FadeOut(self.bitcoins[0], shift=LEFT),
                FadeIn(self.minus_group, shift=RIGHT)
            ), add_new_scene=False)
        self.play(
            AnimationGroup(
                self.complex_plane.add_line(positive=False, real=True, return_animation=True),
                self.complex_plane.add_tick(-1, return_animation=True),
                ApplyMethod(self.speech_bubble.write, 'Thanks!')
            ), add_new_scene=False)
        self.play(
            AnimationGroup(
                self.complex_plane.add_ticks([-2, -3, -4, -5],
                                             return_animation=True),
                ns_anim
            )
        )

    def part14_interlude_how_to_place_numbers(self):
        self.clear()
        self.set_header('Interlude\\\\How to place number on the number line',
                        max_width='30em')
        self.set_text(r'''
For any new number $x$ and any already existing number $y$ exactly one of the following must be true:

\begin{itemize}
\item $x < y$
\item $x = y$
\item $x > y$
\end{itemize}         
        ''', markdown=False)

        self.wait()

    def part15_introducing_darla(self):
        self.clear()
        self.set_header('Chocolate!')

        self.chocolates = Group(*[Chocolate() for i in range(4)])
        self.chocolates.arrange_in_grid(2, 2)
        self.chocolates.center()
        self.chocolates.match_y(self.albert)

        self.darla = Darla()
        self.darla.next_to(self.albert)
        self.darla.to_edge(RIGHT)
        self.darla.match_y(self.albert)
        self.albert.to_edge(LEFT)

        old_complex_plane = self.complex_plane
        self.complex_plane = ComplexPlane()
        self.complex_plane.add_line(positive=True, real=True)
        self.complex_plane.add_line(positive=False, real=True)
        self.complex_plane.shift(self.numberline_shift)

        self.play(
            AnimationGroup(
                FadeIn(VGroup(*self.complex_plane.get_current_lines())),
                self.complex_plane.add_ticks(range(-5, 6), return_animation=True),
                FadeIn(self.albert, shift=LEFT),
                FadeIn(self.darla, shift=RIGHT),
            ), add_new_scene=False)
        self.play(
            AnimationGroup(
                FadeIn(self.chocolates, shift=UP),
                self.complex_plane.set_arrow(4, return_animation=True)
            )
        )

    def part16_share_the_chocolate(self):
        ns_anim = self.set_numberset('Q')

        self.play(
            ApplyMethod(Group(self.chocolates[0:2]).next_to, self.albert),
            ApplyMethod(Group(self.chocolates[2:4]).next_to, self.darla, LEFT),
            self.complex_plane.set_arrow(2, return_animation=True)
        )

        old_chocolates = self.chocolates
        self.chocolates = Group(*[Chocolate() for i in range(3)])
        self.chocolates.arrange_in_grid(1, 3)
        self.chocolates.center()
        self.chocolates.match_y(self.albert)

        self.play(
            FadeOut(old_chocolates, shift=DOWN),
            FadeIn(self.chocolates, shift=UP),
            self.complex_plane.set_arrow(3, return_animation=True)
        )

        self.play(
            AnimationGroup(
                ApplyMethod(self.chocolates[0].next_to, self.albert),
                ApplyMethod(self.chocolates[2].next_to, self.darla, LEFT),
                self.complex_plane.set_arrow(1, return_animation=True)
            ), add_new_scene=False)
        self.play(Indicate(self.chocolates[1]))

        half_chocolates = Group(LeftHalfChocolate(), RightHalfChocolate())
        half_chocolates.arrange_in_grid(1, 2, buff=0)
        half_chocolates.match_x(self.chocolates[1])
        half_chocolates.match_y(self.chocolates[1])

        self.remove(self.chocolates[1])
        self.add(half_chocolates)

        self.play(
            AnimationGroup(
                ApplyMethod(half_chocolates[0].next_to, self.chocolates[0]),
                ApplyMethod(half_chocolates[1].next_to, self.chocolates[2], LEFT),
                self.complex_plane.set_arrow(1.5, return_animation=True),
                self.complex_plane.add_tick(1.5, tick_label='1\\sfrac{1}{2}', return_animation=True, extra_number_space=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)
            ), add_new_scene=False)
        self.play(
            AnimationGroup(
                ns_anim,
                self.complex_plane.add_ticks(
                    [-(1 / 2), 2 + (3 / 8)],
                    tick_labels=['-\\sfrac{1}{2}', '2\\sfrac{3}{8}'],
                    return_animation=True,
                    extra_number_space=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER
                )
            )
        )

        self.to_remove = half_chocolates

    def part17_square(self):
        self.set_header('Exponentials')

        self.play(
            AnimationGroup(
                FadeOut(Group(self.darla, self.albert, self.to_remove,
                              self.chocolates[0], self.chocolates[2])),
                self.complex_plane.remove_arrow(return_animation=True),
            ), add_new_scene=False)

        self.set_text(r'3*3 &= 9 \\ ',
                      r'3^{2} &= 9',
                      **self.big_math_kwargs)

        self.play(Write(self._slide_text))

        self.set_header('The reverse...')

        self.set_text(r'9 &= 3*3 \\ ',
                      r'\sqrt{9} &= 3',
                      **self.big_math_kwargs)

        extra_text = SlideText('These number are already on the numberline!')
        extra_text.next_to(self._slide_text, DOWN, buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER*3)

        self.play(Write(self._slide_text))

        self.play(FadeIn(extra_text))

        self.remove(extra_text)

    def part18_root_the_2(self):
        ns_anim = self.set_numberset('R')
        self.set_header('So... what now?')

        self.set_text(r'\sqrt{2} &= \mathord{?}', **self.big_math_kwargs)

        self.wait()

        self.set_header('Lets try to find a place for $\\sqrt{2}$...')
        self.set_text(r'x * x = 2', **self.big_math_kwargs)

        self.wait()

        old_text = self._slide_text

        self.set_text(r'x * y &= 2 \\ ',
                      r'y &= \frac{2}{x}',
                      add_to_scene=False,
                      **self.big_math_kwargs)

        self.play(ReplacementTransform(old_text, self._slide_text[0]),
                  Write(self._slide_text[1]))

        rect_scale_factor = 2
        rect = Rectangle(width=2, height=1, stroke_width=12)
        rect.scale(rect_scale_factor)
        rect.center()
        rect.shift((0, 1, 0))

        width_variable = DecimalNumber(0, num_decimal_places=4)
        width_variable.add_updater(
            lambda w: w.set_value(rect.width / rect_scale_factor).next_to(rect, DOWN)
        )

        height_variable = DecimalNumber(0, num_decimal_places=4)
        height_variable.add_updater(
            lambda h: h.set_value(rect.height / rect_scale_factor).next_to(rect, LEFT)
        )

        self.play(
            FadeIn(Group(rect, width_variable, height_variable)),
            self.complex_plane.set_arrow(2, return_animation=True),
            FadeOut(self._slide_text)
        )

        w_tracker = ValueTracker(2)
        w_tracker.add_updater(
            lambda w_t: rect.stretch_to_fit_width(w_tracker.get_value() * rect_scale_factor).stretch_to_fit_height((2 / w_tracker.get_value()) * rect_scale_factor)
        )

        for idx in range(3):
            new_value = (rect.width + rect.height) / 2 / rect_scale_factor
            self.play(
                AnimationGroup(
                    w_tracker.animate.set_value(new_value),
                    self.complex_plane.set_arrow(new_value, return_animation=True)
                ), add_new_scene=False)
            self.play(FadeIn(Mobject()))

        self.play(
            FadeOut(
                Group(
                    self.complex_plane.ticks['1.5'],
                    self.complex_plane.numbers['1.5'],
                    self.complex_plane.ticks[str(2+(3/8))],
                    self.complex_plane.numbers[str(2+(3/8))]
                )
            ),
            self.complex_plane.add_tick(math.sqrt(2), tick_label='\\sqrt{2}', return_animation=True, extra_number_space=-3 * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER),
            ns_anim
        )

        self.complex_plane.remove_tick(1.5)
        self.complex_plane.remove_tick(2+(3/8))

        self.play(
            self.complex_plane.add_ticks(
                [-math.sqrt(6), math.sqrt(19), math.pi], tick_labels=['-\\sqrt{6}', '\\sqrt{19}', '\\pi'],
                return_animation=True, extra_number_space=-3 * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER
            )
        )

        self.to_remove = Group(rect, width_variable, height_variable)

    def part19_always_a_free_spot(self):
        self.set_header('There was always a free spot on the number line')

        old_complex_plane = self.complex_plane
        self.complex_plane = ComplexPlane()
        self.complex_plane.add_line(positive=True, real=True)
        self.complex_plane.add_line(positive=False, real=True)
        self.complex_plane.shift(self.numberline_shift)

        self.play(
            AnimationGroup(
                FadeOut(Group(*old_complex_plane.get_family())),
                FadeOut(self.to_remove),
                FadeIn(VGroup(*self.complex_plane.get_current_lines()))
            ), add_new_scene=False)
        self.play(
            self.complex_plane.add_ticks(range(-5, 6), return_animation=True),
            add_new_scene=False)
        self.play(
            self.complex_plane.add_ticks(
                [-7 / 2, 1 / 3],
                tick_labels=['-\\sfrac{7}{2}',
                             '\\sfrac{1}{3}'],
                extra_number_space=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER,
                return_animation=True
            ), add_new_scene=False)
        self.play(
            self.complex_plane.add_ticks(
                [-math.sqrt(6), math.sqrt(19), math.pi, math.e],
                tick_labels=['-\\sqrt{6}', '\\sqrt{19}', '\\pi', 'e'],
                extra_number_space=-3 * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER,
                return_animation=True
            )
        )


    def part20_quadratic_equations(self):
        self.clear()
        self.set_header('Quadratic Equations')

        self.set_text('x^{2} &= 4 \\\\', 'x &= ', '\\mathord{?}',
                      **self.big_math_kwargs)

        self.play(Write(self._slide_text))

        old_text = self._slide_text
        self.set_text('x^{2} &= 4 \\\\', 'x &= ', '2', add_to_scene=False,
                      **self.big_math_kwargs)

        self.play(ReplacementTransform(old_text, self._slide_text))

        extra_text = SlideText('Is this correct?')
        extra_text.next_to(self._slide_text, DOWN, buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * 5)

        self.play(Write(extra_text))

        old_text = self._slide_text
        self.set_text('x^{2} &= 4 \\\\', 'x &= ', '\\pm 2',
                      add_to_scene=False,
                      **self.big_math_kwargs)
        self._slide_text.match_x(old_text, LEFT)

        self.play(ReplacementTransform(old_text, self._slide_text),
                  FadeOut(extra_text))

        extra_math_text = SlideText('x^{2} &= a \\\\', 'x &= \\pm\\sqrt{a}',
                                    **self.big_math_kwargs)

        extra_math_text.next_to(self._slide_text, DOWN,
                                buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * 4)
        extra_math_text.match_x(self._slide_text, LEFT)

        self.play(Write(extra_math_text))

        old_text = self._slide_text
        self._slide_text = extra_math_text
        extra_math_text = SlideText('x^{2} &= -4 \\\\',
                                    'x &= \\pm\\sqrt{-4} \\\\',
                                    'x &= \\mathord{?}',
                                    **self.big_math_kwargs)

        extra_math_text.next_to(old_text, DOWN,
                                buff=DEFAULT_MOBJECT_TO_MOBJECT_BUFFER * 4)
        extra_math_text.match_x(old_text, LEFT)

        self.set_header('Uncharted Territory')

        self.play(
            AnimationGroup(
                FadeOut(old_text),
                ApplyMethod(self._slide_text.match_y, old_text, UP)
            ), add_new_scene=False)
        self.play(Write(extra_math_text))

        old_text = self._slide_text
        self.set_header("Let's make this a bit easier...")
        self.set_text(r'x &= \sqrt{-4} \\ ',
                      r'x &= \sqrt{4*-1} \\ ',
                      r'x &= \sqrt{4} * \sqrt{-1} \\ ',
                      r'x &= 2 * \sqrt{-1}',
                      **self.big_math_kwargs)

        self.remove(self._slide_text)

        self.play(
            FadeOut(Group(old_text, extra_math_text)),
            Write(self._slide_text[0])
        )

        for idx in range(3):
            self.play(Write(self._slide_text[idx+1]))

    def part21_back_to_the_numberline(self):
        self.clear()
        self.set_header('The real challenge')

        self.set_text(r'x &= 2 * \sqrt{-1}', **self.big_math_kwargs)

        extra_text = SlideTextMarkdown('* We can find 2 on the number line\n'
                                       '* But where is $\\sqrt{-1}$?')

        extra_text.next_to(self._slide_text, DOWN,
                           buff=3*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER,
                           coor_mask=np.array([0, 1, 0]))

        self.play(
            FadeIn(Group(*self.complex_plane.get_family())),
            Write(self._slide_text),
            Write(extra_text)
        )

        self.to_remove = Group(extra_text, self._slide_text)

        self.set_text('* There is no way to place $\\sqrt{-1}$ on the line of real numbers.\n'
                      '* All the possible places are taken or do not make sense.\n'
                      '* The only possibility is to add a new number line.\n'
                      '* This new number line must be __orthogonal__ to the existing one.\n'
                      '* __Orthogonal__: Absolutely independent.')

        self.play(
            FadeOut(self.to_remove),
            Write(self._slide_text)
        )

    def part22_the_complex_plane(self):
        self.set_header('Introducing the complex plane')
        ns_anim = self.set_numberset('C')

        ticks_to_remove = [-7/2, -math.sqrt(6), 1/3, math.e, math.pi, math.sqrt(19)]
        tick_remove_group = Group()

        for tick in ticks_to_remove:
            if str(tick) in self.complex_plane.ticks:
                tick_remove_group.add(self.complex_plane.ticks[str(tick)])
                tick_remove_group.add(self.complex_plane.numbers[str(tick)])

        for tick in ticks_to_remove:
            self.complex_plane.remove_tick(tick)

        self.play(
            AnimationGroup(
                FadeOut(self._slide_text),
                FadeOut(tick_remove_group, shift=UP),
            ), add_new_scene=False)
        self.play(ApplyMethod(self.complex_plane.shift, -self.numberline_shift*0.9))

        self.play(
            AnimationGroup(
                FadeIn(self.complex_plane.add_line(positive=True, real=False, return_line=True)),
                FadeIn(self.complex_plane.add_line(positive=False, real=False, return_line=True)),
            ),
            add_new_scene=False)
        self.play(
            AnimationGroup(
                self.complex_plane.add_ticks(range(-5, 6), imag=True,
                                             imag_suffix='\\sqrt{-1}',
                                             exclude_zero=True,
                                             return_animation=True),
                ns_anim
            )
        )

        old_plane = self.complex_plane
        self.complex_plane = ComplexPlane()
        self.complex_plane.create_complex_plane()
        self.complex_plane.match_y(old_plane)

        self.set_text('\\sqrt{-1}=i', **self.big_math_kwargs)
        self._slide_text.to_edge(LEFT)

        pointer_args = (
            {
                'number': 2+3j,
                'arrow_angle': 20*DEGREES
            },
            {
                'number': -4+1j,
                'arrow_angle': 160*DEGREES,
                'align_direction': LEFT
            },
            {
                'number': 3+-5j,
                'arrow_angle': 20*DEGREES
            }
        )

        pointers = VGroup()
        for cur_pointer_args in pointer_args:
            pointers.add(self.complex_plane.get_pointer_to_number(
                **cur_pointer_args))


        self.play(
            AnimationGroup(
                ReplacementTransform(old_plane, self.complex_plane),
                Write(self._slide_text)
            ), add_new_scene=False)
        self.play(FadeIn(pointers))

    def part23_pi_rad_excursion(self):
        self.clear()
        self.set_header('"Units" for angles')
        self.set_text('* "Degrees": 0\\textdegree - 360\\textdegree\n'
                      '    * Arbitrary pseudounit\n'
                      '    * Why not from 0\\textdegree - 400\\textdegree?\n'
                      '* "Rad" or "radians": 0 - 2*$\pi$\n'
                      '    * Ratio of the radius and the length of the circumference until angle is reached\n'
                      '    * Based on the natural relationships in the circle\n'
                      '    * Easier to use in actual computations')

        self.add(self._slide_text)
        self.wait()

        self.clear()
        self.set_header('Introducing the Unit Circle')

        circle_scale = 2
        unit_circle = Circle(color=GREEN, radius=circle_scale,
                             stroke_width=12)
        radius_line = Arrow(ORIGIN, RIGHT*circle_scale,
                            color=RED, stroke_width=12,
                            buff=0)
        radius_brace = Brace(radius_line, direction=DOWN, buff=0.1, color=RED)
        radius_brace_text = MathTex('r=1', color=RED)
        radius_brace.put_at_tip(radius_brace_text)
        circumference_text = MathTex('c=2\\pi', '*', 'r', color=GREEN)
        circumference_text.next_to(unit_circle, UP + LEFT, buff=-0.5)
        arc = Arc(angle=0, color=BLUE, stroke_width=12, radius=circle_scale)

        angle_variable = FlexibleVariable(0, 'rad',
                                          DecimalNumber(0,
                                                        unit='* \\pi',
                                                        color=BLUE),
                                          color=BLUE)
        degree_variable = FlexibleVariable(0, 'degree',
                                           DecimalNumber(0,
                                                         unit='^{\\circ}',
                                                         num_decimal_places=0))
        degree_variable.to_corner(DOWN + LEFT)
        degree_variable.shift(np.array([0, 1, 0]))
        degree_variable.value.add_updater(
            lambda v: v.set_value(math.ceil(angle_variable.tracker.get_value() * 180))).next_to(
            degree_variable.label, RIGHT
        )

        angle_variable.next_to(degree_variable, UP)
        angle_variable.match_x(degree_variable, LEFT)

        radius_line.add_updater(
            lambda l: l.set_angle(angle_variable.tracker.get_value() * math.pi)
        )

        this_plane = ComplexPlane(numbers_per_unit=1/circle_scale)
        this_plane.create_complex_plane(imag_suffix='')
        this_plane.remove_tick(0)

        unit_circle.shift(this_plane.current_shift)
        radius_line.shift(this_plane.current_shift)
        arc.shift(this_plane.current_shift)

        def update_arc(a):
            a.angle = angle_variable.tracker.get_value() * math.pi
            a.generate_points()
            a.shift(this_plane.current_shift)

            return a

        arc.add_updater(update_arc)

        self.play(Create(this_plane))
        self.play(
            Create(
                VGroup(unit_circle,
                       radius_line,
                       radius_brace,
                       radius_brace_text,
                       circumference_text,
                       )
            )
        )

        old_c_text = circumference_text
        inter_circumference_text = MathTex(r'c=2\pi', '*', '1').align_to(old_c_text, UP + LEFT).match_style(old_c_text)
        circumference_text = MathTex(r'c=2\pi').align_to(old_c_text, UP + LEFT).match_style(old_c_text)

        self.play(ReplacementTransform(old_c_text, inter_circumference_text), add_new_scene=False)
        self.play(ReplacementTransform(inter_circumference_text, circumference_text))

        self.play(
            Create(
                VGroup(
                    angle_variable,
                    degree_variable,
                    arc
                )
            ),
            FadeOut(
                VGroup(
                    radius_brace,
                    radius_brace_text
                )
            )
        )

        for cur_angle in np.arange(0, 2, 0.5) + 0.5:
            self.play(
                ApplyMethod(angle_variable.tracker.set_value, cur_angle,
                            run_time=2)
            )

        self.to_remove = Group(
            angle_variable,
            degree_variable,
            unit_circle,
            this_plane,
            radius_line,
            arc,
            circumference_text
        )

    def part24_represent_complex_numbers(self):
        self.set_header('How to represent a complex number',
                        font_size=48, max_width='20em')

        old_plane = self.complex_plane
        self.complex_plane = ComplexPlane()
        self.complex_plane.create_complex_plane()
        self.complex_plane.match_y(old_plane)

        self.play(AnimationGroup(
            FadeOut(self.to_remove),
            FadeIn(self.complex_plane)
        ),
            add_new_scene=False)
        self.play(FadeIn(Mobject()))

        initial_number = complex(1)
        other_numbers = [2+3j, 1j, -2+2j, -3+-3j, 4+-1.5j, 3+2j]
        angle_radius = 1

        complex_number_variable = ColoredComplexNumber(initial_number, real_color=RED, imag_color=BLUE)
        complex_number_tracker = ComplexValueTracker(initial_number)

        angle_variable = FlexibleVariable(
            np.angle(initial_number) / math.pi,
            '\\varphi',
            DecimalNumber(np.angle(initial_number) / math.pi,
                          unit='* \\pi',
                          color=YELLOW),
            color=YELLOW
        )

        length_variable = FlexibleVariable(
            abs(initial_number),
            'r',
            DecimalNumber(abs(initial_number), color=GREEN),
            color=GREEN
        )

        complex_number_variable.to_corner(UP + LEFT)
        complex_number_variable.next_to(self.sbdl_logo, DOWN,
                                        coor_mask=[0, 1, 0],
                                        buff=4*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)
        length_variable.to_corner(DOWN + LEFT)
        length_variable.shift(np.array([0, 2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER, 0]))
        angle_variable.next_to(length_variable, RIGHT)

        real_arrow = BetterVector(start=self.complex_plane.n2p(0), end=self.complex_plane.n2p(initial_number.real), color=RED, buff=0)
        imag_arrow = BetterVector(start=self.complex_plane.n2p(0), end=self.complex_plane.n2p(0.001j), color=BLUE, buff=0)
        result_arrow = BetterVector(start=self.complex_plane.n2p(0), end=self.complex_plane.n2p(initial_number), color=GREEN, buff=0)
        angle = Arc(radius=self.complex_plane.n2p(angle_radius)[0], angle=np.angle(initial_number), color=YELLOW)

        def angle_updater(a):
            a.angle = np.mod(np.angle(complex_number_tracker.get_value()), 2*np.pi)
            a.generate_points()
            a.shift(self.complex_plane.current_shift)

            return a

        real_arrow.add_updater(
            lambda a: a.set_points(endpoint=self.complex_plane.n2p(complex_number_tracker.get_value().real))
        )

        imag_arrow.add_updater(
            lambda a: a.set_points(startpoint=self.complex_plane.n2p(complex_number_tracker.get_value().real),
                                   endpoint=self.complex_plane.n2p(
                                       complex_number_tracker.get_value()))
        )

        result_arrow.add_updater(
            lambda a: a.set_points(endpoint=self.complex_plane.n2p(
                complex_number_tracker.get_value()))
        )

        angle.add_updater(angle_updater)
        angle_variable.add_updater(
            lambda a: a.tracker.set_value(np.mod(np.angle(complex_number_tracker.get_value()), 2*np.pi) / math.pi)
        )

        length_variable.add_updater(
            lambda l: l.tracker.set_value(abs(complex_number_tracker.get_value()))
        )

        complex_number_variable.add_updater(
            lambda c: c.set_value(complex_number_tracker.get_value())
        )

        self.add(
            result_arrow,
            real_arrow,
            angle,
            angle_variable,
            length_variable
        )

        self.play(
            FadeIn(complex_number_variable),
            FadeIn(
                Group(
                    real_arrow,
                    result_arrow,
                    angle,
                    angle_variable,
                    length_variable
                )
            )
        )

        self.add(imag_arrow)

        for cur_number in other_numbers:
            self.play(ApplyMethod(complex_number_tracker.set_value, cur_number), add_new_scene=False)
            self.play(FadeIn(Mobject()))

        cartesian_brace = Brace(complex_number_variable)
        cartesian_brace_text = cartesian_brace.get_text('Cartesian Form').scale(0.8)
        polar_brace = Brace(VGroup(length_variable, angle_variable), UP)
        polar_brace_text = polar_brace.get_text('Polar Form').scale(0.8)

        self.play(
            FadeIn(
                VGroup(
                    cartesian_brace,
                    cartesian_brace_text,
                    polar_brace,
                    polar_brace_text
                )
            )
        )

    def part25_cartesian_and_polar(self):
        circle_scale = 2.5
        angle_radius = 0.75
        angle = 1/5 * math.pi
        angle_text_scale = 0.6

        c_number = np.exp(angle*1j)

        self.clear()
        self.set_header('The cartesian and polar form are equivalent')

        self.complex_plane = ComplexPlane(numbers_per_unit=1/circle_scale,
                                          upper_buff=2)
        self.complex_plane.create_complex_plane()

        real_arrow = BetterVector(self.complex_plane.n2p(0), self.complex_plane.n2p(c_number.real), color=RED)
        imag_arrow = BetterVector(self.complex_plane.n2p(c_number.real), self.complex_plane.n2p(c_number), color=BLUE)
        total_arrow = BetterVector(self.complex_plane.n2p(0), self.complex_plane.n2p(c_number), color=GREEN)
        angle_object = Arc(angle=angle, radius=angle_radius, color=YELLOW)
        angle_object.shift(self.complex_plane.current_shift)
        phi_text = MathTex('\\varphi', color=YELLOW).scale(angle_text_scale)
        phi_pos = (angle_radius/4) * np.exp(angle/2*1j)
        phi_pos_origin = self.complex_plane.n2p(phi_pos)
        phi_text.set_x(phi_pos_origin[0])
        phi_text.set_y(phi_pos_origin[1])
        right_angle = Arc(angle=math.pi/2, radius=angle_radius, start_angle=math.pi/2)
        right_angle.shift(self.complex_plane.current_shift)
        right_angle.shift(np.array([self.complex_plane.n2p(c_number)[0], 0, 0]))

        right_angle_dot_pos = (angle_radius/4) * np.exp(math.pi/4*1j)
        right_angle_dot = MathTex('\\cdot').scale(2)
        right_angle_dot.set_x(self.complex_plane.n2p(c_number)[0] - self.complex_plane.n2p(right_angle_dot_pos)[0])
        right_angle_dot.set_y(self.complex_plane.n2p(right_angle_dot_pos)[1])

        r_text = MathTex('r', color=GREEN).scale(angle_text_scale)
        r_text.next_to(total_arrow, ORIGIN)
        r_text.shift(np.array([-1, 1, 0]) * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        real_text = MathTex('r * \\cos(\\varphi)', color=RED).scale(angle_text_scale)
        real_text.next_to(real_arrow, DOWN)

        imag_text = MathTex('r * \\sin(\\varphi)', color=BLUE).scale(angle_text_scale)
        imag_text.next_to(imag_arrow, RIGHT)

        self.play(
            FadeIn(self.complex_plane),
            FadeIn(
                VGroup(
                    real_arrow,
                    imag_arrow,
                    total_arrow,
                    angle_object,
                    phi_text,
                    right_angle,
                    right_angle_dot,
                    r_text,
                )
            )
        )

        self.play(
            FadeIn(
                VGroup(
                    real_text,
                    imag_text
                )
            )
        )

        formula = MathTex(
            r'c &= r * ',
            r'(\cos(\varphi) + \sin(\varphi)i)',
            r'\\'
            'r &= \\sqrt{real^{2} + imag^{2}}\\\\'
            '\\varphi &= \\arctan(\\frac{imag}{real})'
        ).scale(0.8)

        formula.to_edge(LEFT)
        formula.shift(np.array([0, 0.8, 0]))

        self.play(Write(formula))

        newformula = MathTex(
            r'c &= r * ',
            r'e^{\varphi i}',
            r'\\'
            'r &= \\sqrt{real^{2} + imag^{2}}\\\\'
            '\\varphi &= \\arctan(\\frac{imag}{real})'
        ).scale(0.8).align_to(formula, DOWN+LEFT)

        self.play(
            ReplacementTransform(formula, newformula)
        )

        self.play(Indicate(VGroup(*newformula[0:2])))

        self.clear()
        self.set_header("Euler's formula")

        self.set_text(r'r * e^{\varphi i} = r * (\cos(\varphi) + \sin(\varphi) i)',
                      **self.big_math_kwargs)

        extra_text = SlideText(r'Yes, that seems weird.\\'
                               r'But it is true.\\'
                               r'Just remember it as a shortcut.',
                               font_size=1.4 * 48)

        extra_text.next_to(self._slide_text, DOWN, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        self.wait()

        self.play(Write(extra_text))

        self.clear()
        self.set_header("Marvel at the beauty of Euler's Identity")

        self.set_text(r'e^{\pi i} = -1', **self.big_math_kwargs)

        self.play(Write(self._slide_text))

    def part26_interim_summary(self):
        self.clear()
        self.set_header('Remember this!')
        self.set_text(r'$a+bi$',  r'$\; = \;$',  r'$r * e^{i\varphi}$', markdown=False)
        self._slide_text.scale(2).center()

        cartesian_brace = Brace(self._slide_text[0], DOWN)
        cartesian_brace_text = cartesian_brace.get_text('Cartesian\\\\Form')
        polar_brace = Brace(self._slide_text[2], DOWN)
        polar_brace_text = polar_brace.get_text('Polar\\\\Form')

        braces = Group(cartesian_brace_text, cartesian_brace,
                       polar_brace, polar_brace_text)

        self.add(braces)

        self.play(Indicate(Group(self._slide_text, braces)))

        self.clear()
        self.set_header('Cartesian and Polar Form')
        self.set_text(r'''
* A complex number is defined by both the cartesian and the polar form
* Both are 100% equivalent
* Working in the cartesian form avoids dealing with angles
    * **They jump: $2*\\pi + 0.1 = 0.1$**
    * Easier/faster for computers
* The polar form is easier to interpret for humans in general
    * Especially when one deals with oscillations
''')

    def part28_the_beauty_of_complex_numbers(self):
        self.clear()
        self.set_header('Defining $i=\\sqrt{-1}$ results in:')
        self.set_text(r'''
* A 2 dimensional system in which we can use concepts like:
    * Coordinates (real / imaginary part)
    * Radius / Length and Angles
* But we can keep everything that works so well from 1D systems:
    * All operations still work without any restrictions:
        * Addition, subtraction, multiplication, division
        * Power, roots....
    * All "laws" still apply:
        * $a + b = b + a$
        * $a * b = b * a$
        * ...        
''', font_size=0.75 * 48, max_width='35em')

        self.wait()

        self.clear()
        self.set_header('$\\sqrt{-1}$? My brain still hurts...')

        self.set_text(r'''
In this case:

* Forget what $i$ means. Forget about the $\sqrt{-1}$
* Just think about it like this:
    * A complex number represents something in 2D space
        * real part: X coordinate
        * imaginary part: Y coordinate
        * $\varphi$: Direction
        * $r$: Length
    * Think about maps or any other coordinate system       
''',
                      alignment='')

        self.wait()

    def part29_addition(self):
        math_scale = 0.8

        self.clear()
        self.set_header('Addition')
        self.complex_plane = ComplexPlane()
        self.complex_plane.create_complex_plane()

        number1 = 1.5+1j
        number2 = 2+3j

        c_sum = number1 + number2

        number1_vector = self.complex_plane.get_vector(0, number1, color=RED)
        number2_vector = self.complex_plane.get_vector(0, number2, color=GREEN)
        result_vector = self.complex_plane.get_vector(0, c_sum)

        general_equation = MathTex(r'(a+bi) + (c+di) = (a+c) + (b+d)i').scale(math_scale)

        general_equation.to_edge(UP + LEFT)
        general_equation.next_to(self.sbdl_logo, DOWN, coor_mask=[0, 1, 0],
                                 buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        concrete_equation1 = MathTex(r'%s + %s' % (self.complex2str(number1), self.complex2str(number2)),
                                     tex_to_color_map={
                                         self.complex2str(number1): RED,
                                         self.complex2str(number2): GREEN
                                     }).scale(math_scale)
        concrete_equation1.next_to(general_equation, DOWN, aligned_edge=LEFT)

        concrete_equation2 = MathTex(r'(%s + %s) + (%s + %s)i' % (self.format_float(number1.real),
                                                                  self.format_float(number2.real),
                                                                  self.format_float(number1.imag),
                                                                  self.format_float(number2.imag)),
                                     tex_to_color_map={
                                         self.format_float(number1.real): RED,
                                         self.format_float(number1.real)[1:]: RED,
                                         self.format_float(number1.imag): RED,
                                         self.format_float(number2.real): GREEN,
                                         self.format_float(number2.imag): GREEN,
                                     }).scale(math_scale)

        concrete_equation2.next_to(concrete_equation1, DOWN, aligned_edge=LEFT)

        result = MathTex(self.complex2str(c_sum)).scale(math_scale)
        result.next_to(concrete_equation2, DOWN, aligned_edge=LEFT)

        self.play(
            FadeIn(self.complex_plane),
            Write(general_equation)
        )

        self.play(
            FadeIn(VGroup(number1_vector, number2_vector)),
            Write(concrete_equation1)
        )

        self.play(
            ApplyMethod(number2_vector.shift, np.array(self.complex_plane.n2p(number1)-self.complex_plane.current_shift)),
            Write(concrete_equation2)
        )

        self.play(
            FadeIn(result_vector),
            Write(result)
        )

    def part30_multiplication(self):
        math_scale = 0.8

        self.clear()
        self.set_header('Multiplication')

        kwargs = deepcopy(self.big_math_kwargs)
        kwargs['tex_environment'] = None

        self.set_text(r'$(a + bi) * (c + di)$ \\',
                      r'$ac + adi + bic + bidi$ \\',
                      r'$ac + adi + bic + bdi^{2}$ \\',
                      r'$ac + adi + bic + bd*-1$ \\',
                      r'$ac + adi + bic - bd$ \\',
                      r'$ac - bd + adi + bci$ \\',
                      r'$(ac - bd) + (ad + bc)i$',
                      add_to_scene=False,
                      **kwargs)

        for cur_text in self._slide_text:
            self.play(Write(cur_text))

        self.clear()
        self.set_header('Multiplication')
        self.complex_plane = ComplexPlane()
        self.complex_plane.create_complex_plane()

        number2 = 2 + 3j
        number1 = 1 + 1j

        product = number1 * number2

        number1_vector = self.complex_plane.get_vector(end=number1, color=RED)
        number2_vector = self.complex_plane.get_vector(end=number2, color=GREEN)
        result_vector = self.complex_plane.get_vector(end=product, color=BLUE)

        general_equation = MathTex('(ac - bd) + (ad + bc)i').scale(math_scale)
        general_equation.next_to(self.sbdl_logo, DOWN, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER,
                                 aligned_edge=LEFT)
        concrete_equation = MathTex(
            '(%s * %s - %s * %s) + (%s * %s + %s * %s)i' % (
                self.format_float(number1.real),
                self.format_float(number2.real),
                self.format_float(number1.imag),
                self.format_float(number2.imag),
                self.format_float(number1.real),
                self.format_float(number2.imag),
                self.format_float(number1.imag),
                self.format_float(number2.real)
            ),
            tex_to_color_map = {
                self.format_float(number1.real): RED,
                self.format_float(number1.imag): RED,
                self.format_float(number2.real): GREEN,
                self.format_float(number2.imag): GREEN,
            }
        ).scale(math_scale)

        concrete_equation.next_to(general_equation, DOWN, aligned_edge=LEFT)

        result_obj = MathTex(self.complex2str(product), color=BLUE).scale(math_scale)
        result_obj.next_to(concrete_equation, DOWN, aligned_edge=LEFT)

        self.play(
            FadeIn(
                Group(
                    self.complex_plane,
                    number1_vector,
                    number2_vector,
                )
            ),
            Write(general_equation)
        )

        self.play(Write(concrete_equation), add_new_scene=False)
        self.play(
            AnimationGroup(
                Write(result_obj),
                FadeIn(result_vector)
            )
        )

        self.remove(self._slide_number_object)
        current_state = self.mobjects.copy()

        self.clear()
        self.set_text('No idea what this does.....\\\\', "Let's try again with the polar form...", markdown=False, font_size=1.4 * 48)
        self._slide_text.center()

        self.wait()
        self.clear()

        self.set_header('Multiplication')

        self.set_text(r'$(r_1 * e^{\varphi_{1} i}) * (r_2 * e^{\varphi_{2} i})$ \\',
                      r'$(r_1 * r_2) * (e^{\varphi_1 i} * e^{\varphi_2 i})$ \\',
                      r'$(r_1 * r_2) * e^{(\varphi_1 + \varphi_2) i}$',
                      add_to_scene=False,
                      **kwargs)

        for cur_text in self._slide_text:
            self.play(Write(cur_text))

        self.clear()

        self.add(*current_state)
        self.remove(general_equation, concrete_equation, result_obj,
                    result_vector)

        general_equation = MathTex(r'(r_1 * r_2) * e^{(\varphi_1 + \varphi_2) i}').scale(math_scale)
        general_equation.next_to(self.sbdl_logo, DOWN,
                                 buff=2 * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER,
                                 aligned_edge=LEFT)

        self.add(general_equation)

        concrete_equation = MathTex(
            '(%s * %s) * e^{(%s + %s)i}' % (
                self.format_float(abs(number1)),
                self.format_float(abs(number2)),
                self.format_float(np.angle(number1)),
                self.format_float(np.angle(number2))
            ),
            tex_to_color_map={
                self.format_float(abs(number1)): RED,
                self.format_float(abs(number2)): GREEN,
                self.format_float(np.angle(number1)): RED,
                self.format_float(np.angle(number2)): GREEN
            }
        ).scale(math_scale)

        concrete_equation.next_to(general_equation, DOWN, aligned_edge=LEFT)

        self.wait()

        self.play(
            Write(concrete_equation)
        )

        result_obj = MathTex(
            '%s ' % (self.format_float(abs(product)), ),
            '* e^{%s i}' % (self.format_float(np.angle(product)), ),
            color=BLUE
        ).scale(math_scale)
        result_obj.next_to(concrete_equation, DOWN, aligned_edge=LEFT)

        result_vector = self.complex_plane.get_vector(end=abs(product), color=BLUE)
        result_vector_tracker = ValueTracker(0)

        result_vector.add_updater(
            lambda v: v.set_angle(result_vector_tracker.get_value())
        )
        self.add(result_vector)

        self.play(
            AnimationGroup(
                Write(result_obj[0]),
                GrowArrow(result_vector)
            ), add_new_scene=False)
        self.play(
            AnimationGroup(
                Write(result_obj[1]),
                ApplyMethod(result_vector_tracker.set_value, np.angle(product))
            )
        )

        self.clear()

        self.set_header('Also the reverse all work')
        self.set_text('* Subtraction: Subtract the real part, subtract the imaginary parts\n'
                      '* Division: Divide the radius, subtract the angle')

        self.wait()


    def part31_complex_conjugate(self):
        self.clear()
        self.set_header('The complex conjugate: $\\bar{a}$')

        self.complex_plane = ComplexPlane()
        self.complex_plane.create_complex_plane()

        number = 5+2j

        normal_vector = self.complex_plane.get_vector(end=number)
        conj_vector = self.complex_plane.get_vector(end=number.conjugate())

        normal_text = MathTex(self.complex2str(number)).scale(0.6)
        normal_text.next_to(normal_vector, UP, buff=0.3*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        conj_text = MathTex(self.complex2str(number.conjugate())).scale(0.6)
        conj_text.next_to(conj_vector, DOWN, buff=0.3*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER)

        text = SlideText('Flip the sign of the imaginary part\\\\Mirrow at the real axis',
                         alignment='\\raggedright')
        text.next_to(self.sbdl_logo, DOWN,
                     buff=4 * DEFAULT_MOBJECT_TO_MOBJECT_BUFFER,
                     aligned_edge=LEFT)

        self.play(
            FadeIn(
                Group(self.complex_plane, normal_vector, text, normal_text)
            )
        )

        self.play(Transform(normal_vector.copy(), conj_vector),
                  FadeIn(conj_text))


    def part32_summary_before_euler(self):
        self.clear()

        self.set_header('To take home...')

        self.set_text(r'''
* Before complex numbers:
    * Only 1 dimensional operations
    * Move to the left, move to the right
* With complex numbers:
    * Represent 2 dimensional concepts
        * X/Y coordinates (real/imaginary part)
        * radius and angle
        * rotation
    * Just by adding $i=\sqrt{-1}$
    * All rules still apply. All operations still work as before. This is different for matrices, for example!
''', font_size=48*0.75)

        self.wait()

        self.set_header('Why is this relevant?')
        self.set_text(r'''
* Almost all modern M/EEG analysis looks at oscillations
* An oscillation has a frequency, an amplitude and a phase
* If you keep the frequency constant:
    * Amplitude $\rightarrow$ Length of the vector
    * Phase $\rightarrow$ Angle of the vector
* Being able to transform between the cartesian form (a+bi) and the polar form (length and angle) makes modern analysis possible.        
''')

        self.wait()

        self.clear()
        self.set_header('Wait, Oscillations?')
        oscillation_image = PNGImage(file_name='oscillation', height=5)
        credit_text = Tex(
            'https://commons.wikimedia.org/wiki/File:Cycles\\_de\\_Hurst.png').scale(
            0.5)
        oscillation_image.center()
        credit_text.to_corner(DOWN + RIGHT,
                              buff=2 * DEFAULT_MOBJECT_TO_EDGE_BUFFER)

        self.add(oscillation_image, credit_text)

        self.wait()
        self.clear()

        self.set_header('What to remember')
        self.set_text(r'''
* Basic properties of complex numbers
* How to represent complex numbers
* Specific and general mathematical operations with complex numbers
* i
* $e^{i}$
* Why this is relevant?
''')

        self.wait()
        self.clear()

        self.set_text('Questions?')
        self._slide_text.center()

        self.wait()

    def part33_euler(self):
        self.clear()

        header_text = ['$r * $', '$e^{\\varphi i} =$',  '$r * $', '$($', '$\\cos(\\varphi) + \\sin(\\varphi)i$', '$)$']
        self.set_header(*header_text)

        self.wait()

        old_header = self._slide_header
        new_header_text = [header_text[1], header_text[4]]
        self.set_header(*new_header_text)

        self.set_text(r'''
* $e^x$, $\sin$ and $\cos$ are all "analytic functions"
* They have some special and in this case useful properties:
    * They can be differentiated an unlimited number of times
    * They can be fully represented by something that is called a "Taylor Series"
''')

        self.play(
            ReplacementTransform(old_header, self._slide_header),
            Write(self._slide_text)
        )

        self.clear()
        self.set_header('The Taylor or Maclaurin Series')

        taylor_n = 5

        taylor_series_latex = SlideText(*self.generate_taylor_series('f', taylor_n), **self.small_math_kwargs)

        self.play(Write(taylor_series_latex))

        self.set_header('Maclaurin series for $e^x$')

        e_series = SlideText(*self.generate_e_taylor_series(taylor_n),
                             **self.small_math_kwargs)

        self.play(ReplacementTransform(taylor_series_latex, e_series))

        extra_text = SlideText('We know that $e\'^x = e^x$')
        extra_text.center()
        extra_text.next_to(e_series, DOWN, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER, coor_mask=[0, 1, 0])

        self.play(Write(extra_text))

        new_e_series = SlideText(*self.generate_e_taylor_series(taylor_n, remove_derivative=True), **self.small_math_kwargs)

        self.play(ReplacementTransform(e_series, new_e_series),
                  FadeOut(extra_text))

        extra_text = SlideText('And $e^0 = 1$')
        extra_text.center()
        extra_text.next_to(e_series, DOWN, buff=2*DEFAULT_MOBJECT_TO_MOBJECT_BUFFER, coor_mask=[0, 1, 0])

        self.play(Write(extra_text))

        final_e_series = SlideText(*self.generate_e_taylor_series(taylor_n, remove_derivative=True, remove_e0=True), **self.small_math_kwargs)
        final_e_series.match_y(new_e_series, DOWN)

        self.play(ReplacementTransform(new_e_series, final_e_series),
                  FadeOut(extra_text))

        self.set_header('We can do the same for $\sin$ and $\cos$')

        sin_e_series = SlideText(*self.generate_sin_taylor_series(taylor_n), **self.small_math_kwargs)
        sin_e_series.next_to(final_e_series, DOWN, aligned_edge=LEFT)

        cos_e_series = SlideText(*self.generate_cos_taylor_series(taylor_n),
                                 **self.small_math_kwargs)
        cos_e_series.next_to(sin_e_series, DOWN, aligned_edge=LEFT)

        self.play(
            Write(sin_e_series),
            Write(cos_e_series)
        )

        i_reminder = MathTex(r'''
i^2 &= -1\\
i^3 &= i^2 * i = -i\\
i^4 &= i^2 * i^2 = -1 * -1 = 1\\
i^5 &= i^4 * i = i        
        ''')

        self.clear()
        self.set_header('A short reminder')

        i_reminder.center()

        self.play(Write(i_reminder))

        def move_and_rescale_i_reminder(o):
            o.scale(0.6)
            o.next_to(self.plus_logo, DOWN, aligned_edge=RIGHT)

            return o

        self.play(
            ApplyFunction(move_and_rescale_i_reminder, i_reminder),
            FadeIn(final_e_series)
        )

        self.set_header('Let\'s plug in the i...')

        self.to_remove = Group(i_reminder)

        e_series_i_text = self.generate_e_taylor_series(taylor_n, remove_derivative=True, remove_e0=True, argument='(xi)')

        e_series_i = SlideText(*e_series_i_text,
                               **self.small_math_kwargs)

        self.to_remove.add(e_series_i)

        self.play(ReplacementTransform(final_e_series, e_series_i))

        new_e_series_text = []
        for idx_t, t in enumerate(e_series_i_text):
            if idx_t < 3:
                new_t = t
            else:
                if '(xi)' in t:
                    cur_n = idx_t - 3
                    new_t = t.replace('(xi)^{%d}' % (cur_n, ),
                                      'x^{%d}i^{%d}' % (cur_n, cur_n))
                else:
                    new_t = t

            new_e_series_text.append(new_t)


        new_e_series = SlideText(*new_e_series_text, **self.small_math_kwargs)
        new_e_series.next_to(e_series_i, DOWN, aligned_edge=LEFT)

        self.to_remove.add(new_e_series)

        self.play(Write(new_e_series))

        e_series_i = new_e_series
        e_series_i_text = new_e_series_text

        new_e_series_text = []
        for t in e_series_i_text:
            new_t = t.replace('i^{2}', '*-1')
            new_t = new_t.replace('i^{3}', '*-i')
            new_t = new_t.replace('i^{4}', '*1')
            new_t = new_t.replace('i^{5}', '*i')
            new_t = new_t.replace('i^{2}', '*-1')

            new_e_series_text.append(new_t)

        new_e_series = SlideText(*new_e_series_text, **self.small_math_kwargs)
        new_e_series.next_to(e_series_i, DOWN, aligned_edge=LEFT)

        self.to_remove.add(new_e_series)
        self.play(Write(new_e_series))

        e_series_i = new_e_series
        e_series_i_text = new_e_series_text

        new_e_series_text = []
        for t in e_series_i_text:
            if not '\\frac' in t:
                new_e_series_text.append(t)
            else:
                if '*-' in t:
                    t = t.replace('+', '-')
                t = t.replace('*-', '')
                t = t.replace('*', '')
                t = t.replace('1', '')

                new_e_series_text.append(t)

        new_e_series_text[-1] = new_e_series_text[-1].replace('+', '\\pm')

        new_e_series = SlideText(*new_e_series_text, **self.small_math_kwargs)
        new_e_series.next_to(e_series_i, DOWN, aligned_edge=LEFT)

        self.to_remove.add(new_e_series)
        self.play(Write(new_e_series))

        e_series_i = new_e_series
        e_series_i_text = new_e_series_text

        e_series_real = []
        e_series_imag = []

        e_series_real.extend(e_series_i_text[0:2])
        e_series_real.append('(')

        first = True
        for t in e_series_i_text[2:-1]:
            if 'i' in t:
                if first:
                    t = t.replace('xi', '(xi')
                e_series_imag.append(t)
            else:
                e_series_real.append(t)

        e_series_real.append('\\pm\\dots)')
        e_series_imag.append('\\pm\\dots)')

        new_e_series_text = e_series_real + e_series_imag

        new_e_series = SlideText(*new_e_series_text, **self.small_math_kwargs)
        new_e_series.next_to(e_series_i, DOWN, aligned_edge=LEFT)

        self.play(Write(new_e_series))

        sin_e_series.next_to(e_series, DOWN, aligned_edge=LEFT)
        cos_e_series.next_to(sin_e_series, DOWN, aligned_edge=LEFT)

        self.play(
            FadeOut(self.to_remove),
            ApplyMethod(new_e_series.match_y, e_series, DOWN)
        )

        e_series_i = new_e_series
        e_series_i_text = new_e_series_text

        new_e_series_text = []
        for idx_t, t in enumerate(e_series_i_text):
            if idx_t < 3:
                new_e_series_text.append(t)
            else:
                new_e_series_text.append(t.replace('i', ''))

        new_e_series_text.append('i')

        new_e_series = SlideText(*new_e_series_text, **self.small_math_kwargs)
        new_e_series.match_y(e_series_i, DOWN)

        self.play(ReplacementTransform(e_series_i, new_e_series))

        self.play(
            Write(sin_e_series),
            Write(cos_e_series)
        )

        last_equation = SlideText('e^{(xi)} = \\cos(x) + \\sin(x)i',
                                  **self.small_math_kwargs)

        last_equation.next_to(cos_e_series, DOWN, aligned_edge=LEFT)

        self.play(
            Write(last_equation)
        )



    def construct(self):
        tex_template = TexFontTemplates.droid_sans

        tex_template.preamble = tex_template.preamble.replace('LGRgreek',
                                                              'LGRgreek, italic')

        tex_template.add_to_preamble(
            r'''
        \usepackage{enumitem}
        \usepackage{xfrac}
        \setlist{topsep=0pt,labelindent=\parindent,leftmargin=*}
            '''
        )

        config.tex_template = tex_template
        config.max_files_cached = 1000

        self.part01_title_page()
        self.part02_overview()
        self.part03_pippi()
        self.part04_albert_and_the_line()
        self.part05_albert_gets_his_first_apple()
        self.part06_the_second_apple_and_the_distance()
        self.part07_the_third_apple()
        self.part08_plustwo_apples()
        self.part09_minus_3_apples()
        self.part10_to_zero()
        self.part11_themoney()
        self.part12_pay_2_bitcoins()
        self.part13_pay_2_more_bitcoins()
        self.part14_interlude_how_to_place_numbers()
        self.part15_introducing_darla()
        self.part16_share_the_chocolate()
        self.part17_square()
        self.part18_root_the_2()
        self.part19_always_a_free_spot()
        self.part20_quadratic_equations()
        self.part21_back_to_the_numberline()
        self.part22_the_complex_plane()
        self.part23_pi_rad_excursion()
        self.part24_represent_complex_numbers()
        self.part25_cartesian_and_polar()
        self.part26_interim_summary()
        self.part28_the_beauty_of_complex_numbers()
        self.part29_addition()
        self.part30_multiplication()
        self.part31_complex_conjugate()
        self.part32_summary_before_euler()
        self.part33_euler()
        self.wait()

    def clear(self):
        super().clear()
        self.add(self.sbdl_logo, self.plus_logo)
        if self.current_numberset is not None:
            self.add(self.current_numberset)

    def set_numberset(self, numberset, remove_old=False):
        old_numberset = self.current_numberset

        self.current_numberset = MathTex('\\mathbb{%s}' % (numberset, ))
        self.current_numberset.to_corner(DOWN + LEFT)

        if old_numberset is not None:
            if remove_old:
                self.remove(old_numberset)
            anim = ReplacementTransform(old_numberset, self.current_numberset)
        else:
            anim = FadeIn(self.current_numberset)

        return anim

    def complex2str(self, n):
        return str(n).replace('j', 'i')

    def format_float(self, n):
        if round(n) == n:
            return '%d' % (n,)

        return '%.1f' % (n,)

    def generate_taylor_series(self, function, n, a=0, argument='x', function_arg_part='(%s)'):
        text_list = [function, function_arg_part % (argument, ) + ' = ']
        text_list.extend([function, function_arg_part % (str(a), )])

        for cur_n in range(1, n+1):
            tmp_list = []
            tmp_list.append('+ \\frac{%s%s' % (function, "'" * cur_n) + function_arg_part % (str(a), ))
            tmp_list.append('%s^{%d}}{%d!}' % (argument, cur_n, cur_n))

            text_list.append(''.join(tmp_list))

        text_list.append(' + \\dots')

        return text_list

    def generate_e_taylor_series(self, n, remove_derivative=False, remove_e0=False, argument='x', **kwargs):
        t_series = self.generate_taylor_series('e', n,
                                               function_arg_part='^{%s}', argument=argument, **kwargs)

        if remove_derivative:
            new_t_series_text = []
            for t in t_series:
                new_t = t.replace('\'', '')
                new_t_series_text.append(new_t)

            t_series = new_t_series_text

        if remove_e0:
            new_t_series_text = []
            first = True

            for idx_t, t in enumerate(t_series):
                if idx_t == 0:
                    new_t = t
                else:
                    new_t = t.replace('e', '')
                    if first:
                        new_new_t = new_t.replace('^{0}', '1')
                        if new_new_t != new_t:
                            first = False
                        new_t = new_new_t

                    else:
                        new_t = new_t.replace('^{0}', '')

                new_t_series_text.append(new_t)

            new_t_series_text[4] = '+ %s' % (argument.replace('(', '').replace(')', ''), )

            t_series = new_t_series_text

        return t_series

    def generate_sin_taylor_series(self, n, argument='x'):
        text_list = ['\\sin', '(%s) =' % (argument, ), '%s ' % (argument, )]
        sign_list = ('+', '-')

        for cur_n in range(1, n + 1):
            cur_e = cur_n*2+1
            cur_sign = sign_list[cur_n % 2]

            cur_str = '%s \\frac{%s^{%d}}{%d!}' % (cur_sign, argument, cur_e, cur_e)

            text_list.append(cur_str)

        text_list.append('\\pm\\dots')

        return text_list

    def generate_cos_taylor_series(self, n, argument='x'):
        text_list = ['\\cos', '(%s) =' % (argument, ), '1']
        sign_list = ('+', '-')

        for cur_n in range(1, n + 1):
            cur_e = cur_n*2
            cur_sign = sign_list[cur_n % 2]

            cur_str = '%s \\frac{%s^{%d}}{%d!}' % (cur_sign, argument, cur_e, cur_e)

            text_list.append(cur_str)

        text_list.append('\\pm\\dots')

        return text_list




def change_imag_suffix(number, new_suffix, plane):
    number.set_value(number.get_value(), unit=new_suffix)
    number.scale(plane.number_scale_val)

    return number

