update_env:
	mamba env update -f environment.yml -p ./.venv

make_scene_draft:
	manim number_line_and_plane.py -ql --save_sections

make_scene_final:
	manim number_line_and_plane.py -qh --save_sections

make_scene_gif:
	manim number_line_and_plane.py -qh -i
